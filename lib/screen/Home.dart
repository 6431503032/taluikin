import 'package:flutter/material.dart';
import 'package:namer_app/constants.dart';
import 'package:namer_app/models/store.dart';
import 'package:namer_app/screen/components/body.dart';
import 'package:namer_app/screen/components/item_card.dart';

import 'detail.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Product> _searchQuery = products;
  Widget buildResults(BuildContext context) {
    final results = _searchQuery.where((product) =>
        product.title.toLowerCase().contains(product.title.toLowerCase()));
    return ListView.builder(
      itemCount: results.length,
      itemBuilder: (context, index) {
        final product = results.elementAt(index);
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailsScreen(product: product),
              ),
            );
          },
          child: ItemCard(
            product: product,
            press: () {},
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      leading: IconButton(
        icon: const Icon(Icons.home),
        color: kTextColor,
        onPressed: () {},
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(
            Icons.search,
            color: kTextColor,
          ),
          onPressed: _showSearchBar,
        ),
        SizedBox(width: kDefaultPaddin / 2),
      ],
    );
  }

  void _showSearchBar() {
    setState(() {
      _searchQuery = products;
    });

    showSearch(
      context: context,
      delegate: CustomSearchDelegate(),
    );
  }
}

class CustomSearchDelegate extends SearchDelegate {
  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
      primaryColor: Colors.white,
      primaryIconTheme: IconThemeData(color: Colors.grey),
      textTheme: TextTheme(
        titleLarge: TextStyle(
          color: Color.fromARGB(255, 0, 0, 0),
          fontSize: 18.0,
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }
}
