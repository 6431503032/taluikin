import 'dart:ffi';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MenuPage extends StatefulWidget {
  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  final CollectionReference _menuCollection =
      FirebaseFirestore.instance.collection('Menu');

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _menuCollection.snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        List<MenuItem> items = [];

        for (var doc in snapshot.data!.docs) {
          String menu = doc['menu'];
          num price = doc['price'];

          MenuItem item = MenuItem(
            menu: menu,
            price: price,
          );
          items.add(item);
        }

        return Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (context, index) {
              MenuItem item = items[index];
              return ListTile(
                title: Text(item.menu),
                subtitle: Text('\$${item.price.toStringAsFixed(2)}'),
              );
            },
          ),
        );
      },
    );
  }
}

class MenuItem {
  final String menu;
  final num price;

  MenuItem({required this.menu, required this.price});
}
