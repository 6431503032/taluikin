import 'package:flutter/material.dart';
import 'package:namer_app/models/store.dart';

import '../../../constants.dart';

class ColorAndSize extends StatelessWidget {
  const ColorAndSize({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("การแสดงระดับคะแนน\n"),
              Row(
                children: const [
                  Icon(
                    Icons.star,
                    size: 20.0,
                    color: Colors.orange,
                  ),
                  Icon(
                    Icons.star,
                    size: 20.0,
                    color: Colors.orange,
                  ),
                  Icon(
                    Icons.star,
                    size: 20.0,
                    color: Colors.orange,
                  ),
                  Icon(
                    Icons.star,
                    size: 20.0,
                    color: Colors.orange,
                  ),
                  Icon(
                    Icons.star,
                    size: 20.0,
                    color: Colors.orange,
                  ),
                  Text(
                    "5.0",
                    style: TextStyle(
                        fontSize: 15.0, fontWeight: FontWeight.normal),
                  )
                ],
              ),
            ],
          ),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: TextStyle(color: kTextColor),
              children: [
                TextSpan(text: "ชื่อร้าน\n"),
                TextSpan(
                  text: product.title,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge
                      ?.copyWith(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorDot({
    Key? key,
    required this.color,
    // by default isSelected is false
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: kDefaultPaddin / 4,
        right: kDefaultPaddin / 2,
      ),
      padding: EdgeInsets.all(2.5),
      height: 24,
      width: 24,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: isSelected ? color : Colors.transparent,
        ),
      ),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      ),
    );
  }
}
