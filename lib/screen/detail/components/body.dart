import 'package:flutter/material.dart';
import 'package:namer_app/constants.dart';
import 'package:namer_app/models/store.dart';

import 'menu_price.dart';
import 'review_and_name.dart';
import 'description.dart';

class Bodys extends StatelessWidget {
  final Product product;

  const Bodys({Key? key, required this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // It provide us total height and width
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: size.height * 0.3),
                  padding: EdgeInsets.only(
                    top: size.height * 0.12,
                    left: kDefaultPaddin,
                    right: kDefaultPaddin,
                  ),
                  // height: 500,
                  decoration: BoxDecoration(//** */
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      ColorAndSize(product: product),
                      SizedBox(height: kDefaultPaddin / 2),
                      Description(product: product),
                      SizedBox(height: kDefaultPaddin / 2),
                      Container(
                        height: 250,
                        decoration: const BoxDecoration(),
                        child: IndexedStack(children: [
                          Container(
                            height: 500,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(255, 223, 223, 223),
                            ),
                            child: Column(
                              children: <Widget>[
                                MenuPage(),
                              ],
                            ),
                          )
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
