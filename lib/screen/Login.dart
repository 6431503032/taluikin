// ignore: file_names
import 'package:flutter/material.dart';
import 'package:namer_app/screen/home.dart';

class Login extends StatefulWidget {
  const Login({super.key});
  @override
  State<Login> createState() => LoginState();
}

class LoginState extends State<Login> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Login Taluikin'),
          centerTitle: true,
        ),
        body: Center(
            // padding: EdgeInsets.all(15),

            child: Column(children: [
          Container(
            height: 150,
            width: 150,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://scontent.fbkk14-1.fna.fbcdn.net/v/t39.30808-6/334257589_151234941160710_3439830726733211410_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHNzhR_A0vzG0JpkZwCLGOrhz0DwV71bKKHPQPBXvVsos0efCLt0b2DEi6pD4GrEIf4yD1HLrHM2oPL8vxJVqWN&_nc_ohc=yYjC-fWRT4AAX-sgCax&_nc_ht=scontent.fbkk14-1.fna&oh=00_AfDUULrzHIk5zyUFM3zOL5SpQQA0eixu0sNaJQeZRs6Jmg&oe=6408F082'),
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.all(15),
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: Column(children: [
                      TextField(
                          controller: emailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            //hintText: "Email or Phonenumber",
                            labelText: "Email",
                            prefixIcon: Icon(Icons.email),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(24),
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(24),
                                borderSide: BorderSide(color: Colors.grey)),
                          )),
                      SizedBox(height: 17),
                      TextField(
                        controller: passwordController,
                        obscureText: true,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          //hintText: "Email",
                          prefixIcon: Icon(Icons.lock),
                          labelText: "Password",
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(24),
                              borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(24),
                              borderSide: BorderSide(color: Colors.grey)),
                        ),
                      ),
                      SizedBox(height: 17),
                      TextButton(
                        onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeScreen()))
                        },
                        style: TextButton.styleFrom(
                            minimumSize: Size(327, 50),
                            // padding: EdgeInsets.all(15),

                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            backgroundColor: Colors.green[400]),
                        child: Text(
                          'Login',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      )
                    ]),
                  )
                ],
              )))
        ])));
  }
}
