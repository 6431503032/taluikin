import 'package:flutter/material.dart';
import 'package:namer_app/constants.dart';
import 'package:namer_app/models/store.dart';
import 'detail/components/body.dart';

class DetailsScreen extends StatelessWidget {
  final Product product;

  const DetailsScreen({
    Key? key,
    required this.product,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: product.color,
      appBar: buildAppBar(context),
      body: Stack(
        children: [
          Image.asset(
            product.image,
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
          ),
          Bodys(product: product),
        ],
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: product.color,
      elevation: 0,
      title: Text('Menu Page'),
      leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.pop(context),
      ),
      actions: <Widget>[SizedBox(width: kDefaultPaddin / 2)],
    );
  }
}
