import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int size, id;
  final Color color;
  Product({
    required this.id,
    required this.image,
    required this.title,
    required this.description,
    required this.size,
    required this.color,
  });
}

List<Product> products = [
  Product(
      id: 1,
      title: "ก๋วยเตี๋ยวน้ำตกคุณหนิง",
      size: 12,
      description: "location and time",
      image: "assets/images/ก๋วยเตี๋ยวน้ำตกคุณหนิง.png",
      color: Color.fromARGB(255, 79, 108, 76)),
  Product(
      id: 2,
      title: "บิ๊กเบิ้มกะเพราถาดจัมโบ้",
      size: 8,
      description: "location and time",
      image: "assets/images/บิ๊กเบิ้มกะเพราถาดจัมโบ้.png",
      color: Color(0xFFD3A984)),
  Product(
      id: 3,
      title: "ร้านนั่งเล่น",
      size: 10,
      description: "location and time",
      image: "assets/images/ร้านนั่งเล่น.png",
      color: Color(0xFF989493)),
  Product(
      id: 4,
      title: "สถานีสุขใจ",
      size: 11,
      description: "location and time",
      image: "assets/images/สถานีสุขใจ.png",
      color: Color(0xFFE6B398)),
  Product(
      id: 5,
      title: "เอกเย็นตาโฟ",
      size: 12,
      description: "location and time",
      image: "assets/images/เอกเย็นตาโฟ.png",
      color: Color(0xFFFB7883)),
  Product(
      id: 6,
      title: "ฮาจิคิว",
      size: 12,
      description: "location and time",
      image: "assets/images/ฮาจิคิว.png",
      color: Color(0xFFFB7883)),
];
